﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Database;
using Call_Center_111.Middlewares;
using Services;
using Repositories;

namespace Call_Center_111
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(options => options.UseSqlServer(@"
                Data Source=DESKTOP-DE77N5D\SQLEXPRESS;
                Initial Catalog=CallCenter;
                User ID=SA;Password=taoyeumay"), ServiceLifetime.Singleton);
            services.AddControllersWithViews();
            //services.DataRepository();
            services.AddHttpContextAccessor();
            services.UserServices();
            //// services.AddSingleton<ProductMiddleware>();
        }
        //Integrated Security=True
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();
            //app.ProductMiddleware();
            //app.AdminMiddleware();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            ///app.UseMyMiddleware();// sử dụng hàm app() để gọi middleware trong lớp configure 
            
        }
    }
}
