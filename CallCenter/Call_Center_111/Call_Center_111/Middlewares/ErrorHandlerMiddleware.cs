﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Call_Center_111.Middlewares
{
    public class ErrorHandlerMiddleware
    {
        private  RequestDelegate _next { get; }
        public ErrorHandlerMiddleware (RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync (HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception e)
            {
                await HandleExceptionAsync(context, e);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception e)
        {
            return context.Response.WriteAsync(e.Message);
        }
    }
}
