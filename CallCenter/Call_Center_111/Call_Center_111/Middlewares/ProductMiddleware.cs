﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Call_Center_111.Middlewares
{
    public class ProductMiddleware : IMiddleware // tạo lớp middleware như sử dụng như 1 dịch vụ
    {
        /*kiểm tra url có là /Product không
         * có hiển thi ra danh sách
         * không thì next sang middleware tiếp theo
         */
        public Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            if(context.Request.Path == "/Product")
            {
                string html = "<h1> đây sẽ hiển thị ra danh sách product.</h1>";
                return context.Response.WriteAsync(html);
            } 
            else
            {
                context.Items.Add("DataProduct", "<h1>đây là dữ liệu bên product chuyển sang</h1>");
                return next(context);
            }    
        }
    }
}
