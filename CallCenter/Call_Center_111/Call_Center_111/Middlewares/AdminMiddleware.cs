﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Call_Center_111.Middlewares
{
    public class AdminMiddleware
    {
        private RequestDelegate _next { get; }
        public AdminMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Path == "/admin")
            {
                await Task.Run(
                  async () => {
                      string html = "<h1>CẤM! KHÔNG DƯỢC TRUY CẬP</h1>";
                      context.Response.StatusCode = StatusCodes.Status403Forbidden;
                      await context.Response.WriteAsync(html);
                  }
                );
            }
            else
            {

                // Thiết lập Header cho HttpResponse
                context.Response.Headers.Add("throughCheckAcessMiddleware", new[] { DateTime.Now.ToString() });

                //Console.WriteLine("CheckAcessMiddleware: Cho truy cập");

                // Chuyển Middleware tiếp theo trong pipeline
                //await _next(context);
                var datafromProduct = context.Items["DataProduct"];
                context.Response.StatusCode = StatusCodes.Status403Forbidden;
                await context.Response.WriteAsync((string)datafromProduct);

            }
        }
    }
}
