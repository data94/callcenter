﻿using Call_Center_111.Middlewares;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Call_Center_111
{
    public static  class MiddlewareExtensions
    {
        public static void UseMyMiddleware(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<ErrorHandlerMiddleware>();
        }
        public static void AdminMiddleware(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<AdminMiddleware>();
        }
        public static void ProductMiddleware(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<ProductMiddleware>();
        }
    }
}
