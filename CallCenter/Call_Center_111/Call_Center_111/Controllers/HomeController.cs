﻿using Call_Center_111.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services;
using System.Diagnostics;

namespace Call_Center_111.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger = null;
        //private readonly IDataBaseService _DataBaseService = null;
        //private readonly IDataBaseRepository _IDataBaseRepository = null;
        private readonly IUserService user;

        public HomeController(ILogger<HomeController> logger, IUserService _user/*IDataBaseRepository Data*/)
        {
            _logger = logger;
            // _DataBaseService = data;
            //_IDataBaseRepository = Data;
            user = _user;
        }

        public IActionResult Index()
        {
            //_DataBaseService.CreateDatabase();
            //_IDataBaseRepository.CreateDatabase();
            ViewData["data"] = "Danh Sach Nguoi Dung";
            return View(user.list()) ;///RedirectToAction("Index", "NguoiDung");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
