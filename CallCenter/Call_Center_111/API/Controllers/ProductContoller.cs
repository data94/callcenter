﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class ProductContoller : Controller
    {
        // GET: ProductContoller
        public ActionResult Index()
        {
            return View();
        }

        // GET: ProductContoller/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ProductContoller/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductContoller/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ProductContoller/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ProductContoller/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ProductContoller/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ProductContoller/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
