﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories
{
    public static class ServiceCollectionExtension
    {
        public static void DataRepository(this IServiceCollection services)
        {
            services.AddSingleton<IDataBaseRepository, DataBaseRepository>();


        }
    }
}
