﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories
{
    public class DataBaseRepository : IDataBaseRepository
    {
        private readonly DataContext DataContext = null;
        public DataBaseRepository(DataContext dataContext)
        {
            DataContext = dataContext;
        }

        public void CreateDatabase()
        {
            DataContext.Database.EnsureCreated();

        }
        public void DropDatabase()
        {
            DataContext.Database.EnsureDeleted();
        }

        public void InsertDB()
        {
            throw new NotImplementedException();
        }

        public void UpdateDB()
        {
            throw new NotImplementedException();
        }

    }
}
