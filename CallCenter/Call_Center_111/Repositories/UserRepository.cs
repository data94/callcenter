﻿using Database;
using Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext Data = null;
        public UserRepository(DataContext _data)
        {
            Data = _data;
        }
        public List<User> list()
        {
            return Data.Users.ToList();
        }
        public int Add(User user)
        {
            Data.Users.Add(user);
            Data.SaveChanges();
            return user.UserId;
        }
        public User GetUserById (int id)
        {
            return Data.Users.SingleOrDefault(a => a.UserId == id);
        }
        public bool Update(User user)
        {
            try
            {
                var u = Data.Users.Find(user.UserId);
                u.UserName = user.UserName;
                u.UserAccount = user.UserAccount;
                u.UserPassword = user.UserPassword;
                u.PermissionId = user.PermissionId;

                Data.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(User user)
        {
            try
            {
                var u = Data.Users.Find(user.UserId);
                u.Delete = true;

                Data.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
