﻿using Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories
{
    public interface IUserRepository
    {
        List<User> list();
        int Add(User user);
        bool Update(User user);
        bool Delete(User user);
        User GetUserById(int id);
    }
}
