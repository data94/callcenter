﻿using System;
using System.Collections.Generic;
using System.Text;
using Database.Model;
using Microsoft.EntityFrameworkCore;

namespace Database
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<User> Users { get; set; }
        private DbSet<Advise> Advises { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Children> Childrens { get; set; }
        public DbSet<Call> Calls { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new Permission());
            modelBuilder.ApplyConfiguration(new User());
            modelBuilder.ApplyConfiguration(new Profile());
            modelBuilder.ApplyConfiguration(new Children());
            modelBuilder.ApplyConfiguration(new Call());
            modelBuilder.ApplyConfiguration(new Advise());
        }
    }
}
