﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    public class Children : BaseEntity, IEntityTypeConfiguration<Children>
    {
        public int ChildId { get; set; }
        public string ChildName { get; set; }

        public int ChildAge { get; set; }
        public bool ChildGender { get; set; }
        public string ChildAddress { get; set; }
        public string ChildStatus { get; set; }
        public bool Delete { get; set; }
        public int ProfileId { get; set; }
        public Profile Profile { get; set; }

        public void Configure(EntityTypeBuilder<Children> builder)
        {
            builder.ToTable("Children");
            builder.HasKey(x => x.ChildId);
            builder.Property(x => x.ChildId).HasColumnName("ChildId").IsRequired();
            builder.Property(x => x.ChildName).HasColumnName("ChildName").HasMaxLength(200);
            builder.Property(x => x.ChildAge).HasColumnName("ChildAge");
            builder.Property(x => x.ChildGender).HasColumnName("ChildGender");
            builder.Property(x => x.ChildAddress).HasColumnName("ChildAddress").HasMaxLength(600);
            builder.Property(x => x.ChildStatus).HasColumnName("ChildStatus").HasMaxLength(50);
            builder.HasOne(x => x.Profile).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_Child_Profile").HasForeignKey("ProfileId");
            builder.Property(a => a.Delete).HasColumnName("Delete").HasDefaultValue(false).IsRequired();

            builder.Property(a => a.createdTime).HasColumnName("createdTime").HasColumnType("date").HasDefaultValue(DateTime.Now);
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date").IsRequired(false);
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int").IsRequired(false);
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int").IsRequired(false);
        }
    }
}
