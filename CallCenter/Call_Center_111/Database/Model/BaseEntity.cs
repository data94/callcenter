﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    public class BaseEntity
    {
        protected DateTime createdTime { get; set; }

        protected int? createdBy { get; set; }

        protected DateTime? updatedTime { get; set; }

        protected int? updatedBy { get; set; }
    }
}
