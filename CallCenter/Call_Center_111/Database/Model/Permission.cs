﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    public class Permission : BaseEntity, IEntityTypeConfiguration<Permission>
    {
        public int PermissionId { get; set; }
        public string PermissionName { get; set; }
        public bool Delete { get; set; }

        public void Configure(EntityTypeBuilder<Permission> builder)
        {
            builder.ToTable("Permission");
            builder.HasKey(x => x.PermissionId);
            builder.Property(x => x.PermissionId).HasColumnName("PermissionId").IsRequired();
            builder.Property(x => x.PermissionName).HasColumnName("PermissionName").HasMaxLength(200);
            builder.Property(a => a.Delete).HasColumnName("Delete").HasDefaultValue(false).IsRequired();

            builder.Property(a => a.createdTime).HasColumnName("createdTime").HasColumnType("date").HasDefaultValue(DateTime.Now);
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date").IsRequired(false);
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int").IsRequired(false);
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int").IsRequired(false);
        }
    }
}
