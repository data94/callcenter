﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    public class Profile : BaseEntity, IEntityTypeConfiguration<Profile>
    {
        public int ProfileId { get; set; }
        public string PhoneNumber { get; set; }
        public bool Delete { get; set; }
        public Children Children { get; set; }

        public void Configure(EntityTypeBuilder<Profile> builder)
        {
            builder.ToTable("Profile");
            builder.HasKey(x => x.ProfileId);
            builder.Property(x => x.ProfileId).HasColumnName("ProfileId").IsRequired();
            builder.Property(x => x.PhoneNumber).HasColumnName("PhoneNumber").HasMaxLength(10);
            builder.Property(a => a.Delete).HasColumnName("Delete").HasDefaultValue(false).IsRequired();

            builder.Property(a => a.createdTime).HasColumnName("createdTime").HasColumnType("date").HasDefaultValue(DateTime.Now);
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date").IsRequired(false);
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int").IsRequired(false);
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int").IsRequired(false);
        }
    }
}
