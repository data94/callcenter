﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Database.Model
{
    public class Call : BaseEntity, IEntityTypeConfiguration<Call>
    {
        public int CallId { get; set; }
        public string CallLink { get; set; }
        public string UserContent { get; set; }
        public string UserInfomation { get; set; }
        public bool Delete { get; set; }
        public int ChildId { get; set; }
        public Children children { get; set; }
        public int UserId { get; set; }
        public User user { get; set; }
        public void Configure(EntityTypeBuilder<Call> builder)
        {
            builder.ToTable("Call");
            builder.HasKey(x => x.CallId);
            builder.Property(x => x.CallId).HasColumnName("CallId").IsRequired();
            builder.Property(x => x.CallLink).HasColumnName("CallLink").HasMaxLength(600);
            builder.Property(x => x.UserContent).HasColumnName("UserContent").HasMaxLength(2000);
            builder.Property(x => x.UserInfomation).HasColumnName("UserInfomation").HasMaxLength(2000);
            builder.HasOne(x => x.children).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_Child_Call").HasForeignKey("ChildId");
            builder.HasOne(x => x.user).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_User_Call").HasForeignKey("UserId");
            builder.Property(a => a.Delete).HasColumnName("Delete").HasDefaultValue(false).IsRequired();

            builder.Property(a => a.createdTime).HasColumnName("createdTime").HasColumnType("date").HasDefaultValue(DateTime.Now);
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date").IsRequired(false);
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int").IsRequired(false);
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int").IsRequired(false);
        }
    }
}
