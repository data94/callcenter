﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Database.Model
{
    public class User : BaseEntity, IEntityTypeConfiguration<User>
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserAccount { get; set; }
        public string UserPassword { get; set; }
        public bool Delete { get; set; }
        public int PermissionId { get; set; }
        public Permission permission { get; set; }

        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");
            builder.HasKey(x => x.UserId);
            builder.Property(x => x.UserId).HasColumnName("UserId").IsRequired();
            builder.Property(x => x.UserName).HasColumnName("UserName").HasMaxLength(200);
            builder.Property(x => x.UserAccount).HasColumnName("UserAccount").HasMaxLength(200);
            builder.Property(x => x.UserPassword).HasColumnName("UserPassword").HasMaxLength(200);
            builder.HasOne(x => x.permission).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("PK_User_Permission").HasForeignKey("PermissionId");
            builder.Property(a=>a.Delete).HasColumnName("Delete").HasDefaultValue(false).IsRequired();

            builder.Property(a => a.createdTime).HasColumnName("createdTime").HasColumnType("date").HasDefaultValue(DateTime.Now);
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date").IsRequired(false);
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int").IsRequired(false);
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int").IsRequired(false);
        }
    }
}
