﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Model
{
    class Advise : BaseEntity, IEntityTypeConfiguration<Advise>
    {
        public int AdviseId { get; set; }
        public string ListenLink { get; set; }
        public string ContentAdvise { get; set; }
        public bool Delete { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }

        public void Configure(EntityTypeBuilder<Advise> builder)
        {
            builder.ToTable("Advise");
            builder.HasKey(x => x.AdviseId);
            builder.Property(x => x.ListenLink).HasColumnName("ListenLink").IsRequired();
            builder.Property(x => x.ContentAdvise).HasColumnName("ContentAdvise").HasMaxLength(5000);
            builder.HasOne(x => x.User).WithMany().OnDelete(DeleteBehavior.NoAction).HasForeignKey(z => z.UserId).HasConstraintName("Khoa_ngoai_nguoidung_tuvan");
            builder.Property(a => a.Delete).HasColumnName("Delete").HasDefaultValue(false).IsRequired();

            builder.Property(a => a.createdTime).HasColumnName("createdTime").HasColumnType("date").HasDefaultValue(DateTime.Now);
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date").IsRequired(false);
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int").IsRequired(false);
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int").IsRequired(false);
        }

    }
}
