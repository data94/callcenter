﻿using Database.Model;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class UserService: IUserService
    {
        private IUserRepository Repostory { get; }
        public UserService (IUserRepository repos)
        {
            Repostory = repos;
        }
        public List<User> list()
        {
            return Repostory.list();
        }
        public int Add(User user)
        {
            return Repostory.Add(user);
        }
        public bool Update(User user)
        {
            return Repostory.Update(user);
        }
        public bool Delete (User user)
        {
            return Repostory.Delete(user);
        }
        public User GetUserById(int id)
        {
            return Repostory.GetUserById(id);
        }
    }
}
