﻿using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class DataBaseService : IDataBaseService
    {
        private IDataBaseRepository Repository { get; }
        public DataBaseService (IDataBaseRepository res)
        {
            Repository = res;
        }
        public void CreateDatabase()
        {
            Repository.CreateDatabase();
        }

        public void DropDatabase()
        {
            Repository.DropDatabase();
        }

        public void InsertDB()
        {
            throw new NotImplementedException();
        }

        public void UpdateDB()
        {
            throw new NotImplementedException();
        }
    }
}
