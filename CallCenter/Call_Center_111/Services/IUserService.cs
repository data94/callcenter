﻿using Database.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public interface IUserService
    {
        List<User> list();
        int Add(User user);
        bool Update(User user);
        bool Delete(User user);
        User GetUserById(int id);
    }
}
