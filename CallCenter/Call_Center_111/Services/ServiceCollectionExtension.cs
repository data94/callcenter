﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public static class ServiceCollectionExtension
    {
        public static void UserServices(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            //services.AddSingleton<ProductService>(); // khi khởi tạo chỉ tạo ra 1 dịch vụ và không thay đổi nếu gọi lại 1 lần nào nữa
            //services.AddTransient<ProductService>();// mỗi lần khởi tạo se tạo ra 1 dịc vụ riêng.


        }
        public static void DataServices(this IServiceCollection services)
        {
            //services.AddScoped<IDataBaseService, DataBaseService>();


        }
    }
}
