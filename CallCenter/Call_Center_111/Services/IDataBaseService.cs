﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public interface IDataBaseService
    {
        void CreateDatabase();
        void DropDatabase();
        void InsertDB();
        void UpdateDB();
    }
}
