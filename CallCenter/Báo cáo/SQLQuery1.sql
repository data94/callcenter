﻿Drop database CallCenter

use CallCenter

insert into Permission (PermissionName) values
(N'Nhân viên'), (N'Admin')
insert into dbo.[User] (UserName, UserAccount, UserPassword, PermissionId) values
(N'Trần Văn Dũng', 'Dxtran8', '123456', 1),
(N'Trần Văn A', 'Account1', '123456', 2),
(N'Trần Văn B', 'Account2', '123456', 2),
(N'Trần Văn C', 'Account3', '123456', 1),
(N'Trần Văn D', 'Account4', '123456', 2),
(N'Trần Văn E', 'Account5', '123456', 1)