﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DB.Entity
{
    public class TreEm : BaseEntity, IEntityTypeConfiguration<TreEm>
    {
        [Key]
        public int MaTreEm { get; set; }
        public string TenTreEm { get; set; }
        public int Tuoi { get; set; }
        public string GioiTinh { get; set; }
        public string DiaChi { get; set; }
        public string TrangThai { get; set; }
        public int MaQuanHe { get; set; }
        public QuanHe QuanHe { get; set; }
        public void Configure(EntityTypeBuilder<TreEm> builder)
        {
            builder.ToTable("TreEm");
            builder.HasKey(a => a.MaTreEm).HasName("MaTreEm");
            builder.Property(a => a.TenTreEm).HasColumnType("nvarchar")
                .HasColumnName("TenTreEm").HasMaxLength(100);
            builder.Property(a => a.Tuoi).HasColumnType("int")
                .HasColumnName("Tuoi").HasMaxLength(200);
            builder.Property(a => a.GioiTinh).HasColumnName("GioiTinh")
                .HasColumnType("nvarchar").HasMaxLength(6);
            builder.Property(a => a.DiaChi).HasColumnName("DiaChi")
                .HasColumnType("nvarchar").HasMaxLength(5000);
            builder.Property(a => a.TrangThai).HasColumnName("TrangThai")
                .HasColumnType("nvarchar").HasMaxLength(100);
            builder.HasOne(a => a.QuanHe)
                .WithOne(a=>a.TreEm).HasForeignKey<TreEm>(c=>c.MaQuanHe)
                .HasConstraintName("Khoa_Ngoai_TreEm_QuanHe")
                .OnDelete(DeleteBehavior.Cascade);//xóa bảng này thì bảng kia có mã quan hệ cũng bị xóa

            builder.Property(a => a.createdBy).HasColumnName("createdTime").HasColumnType("date");
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date");
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int");
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int");
        }
    }
}
