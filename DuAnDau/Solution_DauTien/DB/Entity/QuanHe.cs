﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB.Entity
{
    public class QuanHe : BaseEntity, IEntityTypeConfiguration<QuanHe>
    {
        public int MaQuanHe { get; set; }
        public string HoTen { get; set; }
       /* public DateTime? NgaySinh { get; set; }
        public string GioiTinh { get; set; }*/
        public string SDT { get; set; }
        public string TenQuanHe { get; set; }
        public TreEm TreEm { get; set; }
        public void Configure(EntityTypeBuilder<QuanHe> builder)
        {
            builder.ToTable("QuanHe");
            builder.HasKey(a => a.MaQuanHe).HasName("MaQuanHe");
            builder.Property(a => a.HoTen).HasColumnType("nvarchar")
                .HasColumnName("HoTen").HasMaxLength(100);
           /* builder.Property(a => a.NgaySinh).HasColumnType("date").IsRequired(false)
                .HasColumnName("NgaySinh");
            builder.Property(a => a.GioiTinh).HasColumnName("GioiTinh").IsRequired(false)
                .HasColumnType("nvarchar").HasMaxLength(6);*/
            builder.Property(a => a.SDT).HasColumnName("SDT")
                .HasColumnType("varchar").HasMaxLength(10).IsRequired(true);
            builder.Property(a => a.TenQuanHe).HasColumnName("TenQuanHe")
                .HasColumnType("nvarchar").HasMaxLength(100);

            builder.Property(a => a.createdBy).HasColumnName("createdTime").HasColumnType("date");
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date");
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int");
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int");
        }
    }
}
