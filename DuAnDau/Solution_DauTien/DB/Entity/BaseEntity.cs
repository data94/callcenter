﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB.Entity
{
    public  class BaseEntity /*: IEntityTypeConfiguration<BaseEntity>*/
    {
        protected DateTime createdTime { get; set; }

        protected int createdBy { get; set; }

        protected DateTime updatedTime { get; set; }

        protected int updatedBy { get; set; }

       /* public void Configure(EntityTypeBuilder<BaseEntity> builder)
        {
            builder.Property(a => a.createdBy).HasColumnName("createdTime").HasColumnType("date");
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date");
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int");
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int");
        }*/
    }
}
