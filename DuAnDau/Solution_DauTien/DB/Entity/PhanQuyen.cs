﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB.Entity
{
    public class PhanQuyen : BaseEntity, IEntityTypeConfiguration<PhanQuyen>
    {
        public int MaPhanQuyen { get; set; }
        public string TenPhanQuyen { get; set; }

        public void Configure(EntityTypeBuilder<PhanQuyen> builder)
        {
            builder.ToTable("PhanQuyen");
            builder.HasKey(a => a.MaPhanQuyen).HasName("MaPhanQuyen");
            builder.Property(a => a.TenPhanQuyen).HasColumnType("nvarchar")
                .HasColumnName("TenPhanQuyen").HasMaxLength(200);

            builder.Property(a => a.createdBy).HasColumnName("createdTime").HasColumnType("date");
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date");
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int");
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int");
        }
    }
}
