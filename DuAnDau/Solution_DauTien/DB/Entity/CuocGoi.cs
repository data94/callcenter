﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB.Entity
{
    public class CuocGoi : BaseEntity, IEntityTypeConfiguration<CuocGoi>
    {
        public int MaCuoiGoi { get; set; }
        public string Link { get; set; }
        public string NoiDungVanDe { get; set; }
        public string ThongTinTuVan { get; set; }
        public int MaNguoiDung { get; set; }
        public int MaTreEm { get; set; }
        public NguoiDung NguoiDung { get; set; }
        public TreEm TreEm { get; set; }

        public void Configure(EntityTypeBuilder<CuocGoi> builder)
        {
            builder.ToTable("CuoiGoi");
            builder.HasKey(a => a.MaCuoiGoi).HasName("MaCuocGoi");
            builder.Property(a => a.Link).HasColumnType("nvarchar")
                .HasColumnName("Link").HasMaxLength(5000);
            builder.Property(a => a.NoiDungVanDe).HasColumnName("NoiDungVanDe")
                .HasColumnType("nvarchar").HasMaxLength(1000000);
            builder.Property(a => a.ThongTinTuVan).HasColumnType("Nvarchar")
                .HasColumnName(ThongTinTuVan).HasMaxLength(1000000);
            builder.HasOne(a => a.NguoiDung).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey("MaNguoiDung").HasConstraintName("KhoaNgoai_NguoiDung_CuocGoi");
            builder.HasOne(a => a.TreEm).WithMany().OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("Khoa_Ngoai_TreEm_CuoiGoi").HasForeignKey("MaTreEm");

            builder.Property(a => a.createdBy).HasColumnName("createdTime").HasColumnType("date");
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date");
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int");
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int");
        }
    }
}
