﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB.Entity
{
    public class NguoiDung : BaseEntity, IEntityTypeConfiguration<NguoiDung>
    {
        public int MaNguoiDung { get; set; }
        public string TenNguoiDung { get; set; }
        public string TaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public int MaPhanQuyen { get; set; }
        public PhanQuyen PhanQuyen { get; set; }

        public void Configure(EntityTypeBuilder<NguoiDung> builder)
        {
            builder.ToTable("NguoiDung");
            builder.HasKey(a => a.MaNguoiDung).HasName("MaNguoiDung");
            builder.Property(a => a.TenNguoiDung).HasColumnType("nvarchar")
                .HasColumnName("TenNguoiDung").HasMaxLength(100);
            builder.Property(a => a.TaiKhoan).HasColumnType("varchar")
                .HasColumnName("TaiKhoan").HasMaxLength(200);
            builder.Property(a => a.MatKhau).HasColumnName("MatKhau")
                .HasColumnType("varchar").HasMaxLength(300);
            builder.HasOne(a => a.PhanQuyen)
                .WithMany().HasForeignKey("MaPhanQuyen")
                .HasConstraintName("Khoa_Ngoai_NguoiDung_PhanQuyen")
                .OnDelete(DeleteBehavior.NoAction);

            builder.Property(a => a.createdBy).HasColumnName("createdTime").HasColumnType("date");
            builder.Property(a => a.updatedTime).HasColumnName("updatedTime").HasColumnType("date");
            builder.Property(a => a.createdBy).HasColumnName("createdBy").HasColumnType("int");
            builder.Property(a => a.updatedBy).HasColumnName("updatedBy").HasColumnType("int");
        }
    }
}
