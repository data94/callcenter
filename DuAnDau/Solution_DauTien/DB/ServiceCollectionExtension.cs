﻿using DB.Interface;
using DB.Repositories;
using DB.Repositories.Interface;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB
{
    public static class ServiceCollectionExtension
    {
        public static void PhanquyenServices(this IServiceCollection services)
        {
            services.AddSingleton<IPhanQuyenRepository, PhanQuyenRepository>();
            services.AddSingleton<ICreateDatabase>();

        }
        public static void CreateServices(this IServiceCollection services)
        {
            services.AddSingleton<ICreateDatabase>();

        }
    }
}
