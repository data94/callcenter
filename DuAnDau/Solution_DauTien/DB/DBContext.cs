﻿using DB.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB
{
    public class DBContext:DbContext
    {
        public DbSet<PhanQuyen> PhanQuyens { get; set; }
        public DbSet<QuanHe> QuanHes { get; set; }
        public DbSet<NguoiDung> NguoiDungs { get; set; }
        public DbSet<TreEm> TreEms { get; set; }
        public DbSet<CuocGoi> CuocGois { get; set; }
       
        
        
        protected override void OnModelCreating(ModelBuilder modelbuider)
        {
            base.OnModelCreating(modelbuider);
            modelbuider.ApplyConfiguration(new PhanQuyen());
            modelbuider.ApplyConfiguration(new QuanHe());
            modelbuider.ApplyConfiguration(new NguoiDung());
            modelbuider.ApplyConfiguration(new TreEm());
            modelbuider.ApplyConfiguration(new CuocGoi());
        }
        private const string connectionString = @"
                Data Source=DESKTOP-DE77N5D\SQLEXPRESS;
                Initial Catalog=mydata;
                User ID=SA;Password=taoyeumay";

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(connectionString);
        }
        public bool CreateData()
        {
            var dbcontext = new DBContext();
            return dbcontext.Database.EnsureCreated();
        }
        /*public static async void CreateDatabase()
        {
            using (var dbcontext = new DBContext())
            {
                String databasename = dbcontext.Database.GetDbConnection().Database;

                bool result = await dbcontext.Database.EnsureCreatedAsync();
                string resultstring = result ? "tạo  thành  công" : "đã có trước đó";
                resultstring = $"CSDL {databasename} : {resultstring}";
            }
        }*/
        /*protected override void OnModelCreating(ModelBuilder modelbuider)
        {
            base.OnModelCreating(modelbuider);

            //Fluent API không nên dùng nếu Attribute có thể dùng được
            // chỉ dùng fluent api co các mối quan hệ 1-1 mà attribute không thể làm được
            modelbuider.Entity<Product>(entity =>
            {
                entity.ToTable("Product");
                entity.HasKey(p => p.ID);
                entity.HasOne(p => p.Brand) // phaanf mootj
                .WithMany()                 // phan nhieu
                .HasForeignKey("IdBrand")    // tên thuộc tính khoa ngoai
                .OnDelete(DeleteBehavior.NoAction) // taọ ràng buộc nếu xóa bên 1 thì bên nhiều có bị xóa hay ko
                .HasConstraintName("Khoa_Ngoai_Product_Brand")  // tên constrain liên kết.
                ;
                entity.Property(p => p.Name)
                .HasColumnName("Name")
                .HasColumnType("nvarchar")
                .HasMaxLength(100)
                .IsRequired(false)  // true thì not null, false thì là có thẻ null
                //.HasDefaultValue("Tên mặc đinh") nếu muốn nó có giá trị mặc định
                ;
            });
        }*/
        /*public static async void CreateDatabase()
        {
            using (var dbcontext = new QLSanPhamDBContext())
            {
                String databasename = dbcontext.Database.GetDbConnection().Database;

                bool result = await dbcontext.Database.EnsureCreatedAsync();
                string resultstring = result ? "tạo  thành  công" : "đã có trước đó";
                resultstring = $"CSDL {databasename} : {resultstring}";
            }
        }
        public static async void DeleteDatabase()
        {

            using (var context = new QLSanPhamDBContext())
            {
                String databasename = context.Database.GetDbConnection().Database;
                Console.Write($"Có chắc chắn xóa {databasename} (y) ? ");
                string input = Console.ReadLine();

                // Hỏi lại cho chắc
                if (input.ToLower() == "y")
                {
                    bool deleted = await context.Database.EnsureDeletedAsync();
                    string deletionInfo = deleted ? "đã xóa" : "không xóa được";
                    deletionInfo = $"{databasename} {deletionInfo}";
                }
            }

        }*/
    }
}
