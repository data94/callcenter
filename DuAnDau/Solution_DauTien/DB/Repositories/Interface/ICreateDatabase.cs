﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB.Repositories.Interface
{
    public interface ICreateDatabase
    {
        bool CreateData();
    }
}
