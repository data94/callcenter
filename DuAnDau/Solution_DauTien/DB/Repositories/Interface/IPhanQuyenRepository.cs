﻿using DB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB.Interface
{
    public interface IPhanQuyenRepository
    {
        List<PhanQuyen> list();
        
    }
}
