﻿using DB.Entity;
using DB.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB.Repositories
{
    public class PhanQuyenRepository : IPhanQuyenRepository
    {
        private DBContext dBContext = null;
        public PhanQuyenRepository(DBContext context)
        {
            this.dBContext = context;
        }
        
        public List<PhanQuyen> list()
        {
            return dBContext.PhanQuyens.ToList();
        }

    }
}
