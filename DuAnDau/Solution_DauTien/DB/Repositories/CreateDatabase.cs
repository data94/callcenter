﻿using DB.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB.Repositories
{
    public class CreateDatabase : ICreateDatabase
    {
        public bool CreateData()
        {
            var dbcontext = new DBContext();
            return dbcontext.Database.EnsureCreated();
        }
    }
}
