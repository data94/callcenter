﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DB.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebAppMVC.Models;
using DB.Repositories;
using DB.Repositories.Interface;

namespace WebAppMVC.Controllers
{
    public class HomeController : Controller
    {
        var data = new DBContext();
        private readonly ILogger<HomeController> _logger;
        private IPhanQuyenRepository _IPhanQuyenRepository { get; }
        private ICreateDatabase _ICreateDatabase { get; }
        public HomeController(ILogger<HomeController> logger, IPhanQuyenRepository IPhanQuyenRepository, ICreateDatabase ICreateDatabase)
        {
            _logger = logger;
            _IPhanQuyenRepository = IPhanQuyenRepository;
            _ICreateDatabase = ICreateDatabase;
        }
        public ActionResult ay()
        { 
            return View();
        }
        public IActionResult Index()
        {
            if(_ICreateDatabase.CreateData())
            { 
                ViewData["122"] = "Tạo dât base thành công"; 
            }  
            else
                ViewData["122"] = "Tạo data base thất bại";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
