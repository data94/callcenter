﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Entity
{
    public class Color :IEntityTypeConfiguration<Color>
    { 
        public int ID { get; set; }
        public string Color1 { get; set; }

        public string Color2 { get; set; }

        public string Color3 { get; set; }
        public virtual Product Product { get; set; }

        public void Configure(EntityTypeBuilder<Color> builder)
        {
            builder.ToTable("Color")
                .HasKey(x => x.ID);
            builder.Property(x => x.ID).HasColumnName("ID");
            builder.Property(x => x.Color1)
                .HasColumnName("Color1")
                .HasColumnType("nvarchar")
                .HasMaxLength(100)
                .IsRequired(false); // true thì not null, false thì là có thẻ null
                //.HasDefaultValue("Tên mặc đinh") nếu muốn nó có giá trị mặc định
        }


        /*yêu cầu phân fmeemf 
         *quản lý người dùng: người dùng nhân viên ko có quyền xóa(họ tên, tài khoan, mật khẩu, phân quyền)
         *quản lý trẻ em:
         *xem đanh sách (tìm kiếm theo loại tình trạng, quận huyện, tỉnh thành phố, tìm kiếm theo sdt)
         *thếm mới, sửa (khi có 1 cuộc gọi tới, SDT, tên tuổi, người giám hộ, quan hệ(ông bà bố mẹ, người thân, khác), địa chỉ, tỉnh quận huyện, xã, địa chỉ cụ thể: nội dung vấn đề; thông tin tư vấn(người tư vấn họ đã nói gì với trẻ em), Link ghi âm , trạng thái, ngày gọi)
         *xóa(chỉ có admin mới đc xóa)
         *nghe lại đoạn ghi âm
         *báo cáo tổng hợp
         *+báo cáo theo thời gian thời gian_ tháng (số lượng trẻ em theo trạng thái đang mở, đang sử lý, đã sử lý)
         *+báo cáo theo đọ tuổi của trẻ(trên 13, trên 16) theo thời gian
         */
    }
}
