﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Entity
{
    public partial class Product :IEntityTypeConfiguration<Product>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double? Price { get; set; }
        public int? IdBrand { get; set; }
        public int? IdColor { get; set; }
        public virtual Brand Brand { get; set; }
        public virtual Color Color { get; set; }
        public Product(int id, string name, double price, int Idbrand, int idcolor)
        {
            this.ID = id;
            this.Name = name;
            this.Price = price;
            this.IdBrand = Idbrand;
            this.IdColor = idcolor;
        }
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Product")
                .HasKey(x => x.ID);
            builder.Property(x => x.ID).HasColumnName("ID");
            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("nvarchar")
                .HasMaxLength(100)
                .IsRequired(false); // true thì not null, false thì là có thẻ null
                                    //.HasDefaultValue("Tên mặc đinh") nếu muốn nó có giá trị mặc định
            builder.Property(x => x.Price)
                .HasColumnName("Price")
                .HasColumnType("float")
                .IsRequired(false);
            builder.HasOne(p => p.Brand) // phaanf mootj
                .WithMany()                 // phan nhieu
                .HasForeignKey("IdBrand")    // tên thuộc tính khoa ngoai
                .OnDelete(DeleteBehavior.NoAction) // taọ ràng buộc nếu xóa bên 1 thì bên nhiều có bị xóa hay ko
                .HasConstraintName("Khoa_Ngoai_Product_Brand");  // tên constrain liên kết.
            builder.HasOne(a => a.Color)
                .WithMany()
                .HasForeignKey("IdColor")
                .OnDelete(DeleteBehavior.NoAction)
                .HasConstraintName("Khoa_Ngoai_Producr_Color");
        }

        
    }
}
