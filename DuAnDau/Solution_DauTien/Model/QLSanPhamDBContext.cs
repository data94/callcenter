﻿using Microsoft.EntityFrameworkCore;
using Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model
{
    public partial class QLSanPhamDBContext:DbContext
    {
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<Color> Colors { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelbuider)
        {
            base.OnModelCreating(modelbuider);
            modelbuider.ApplyConfiguration(new Color());

        }
        
    }
}
